<?php 
namespace Vdmytriv\Wordpress;

use PHPUnit\Framework\TestCase;

/**
 * Override time() in current namespace for testing
 *
 * @return int
 */
function time()
{
    return WpNoncesTest::$now ?? \time();
}

class WpNoncesTest extends TestCase
{
    /**
     * @var int $now Timestamp that will be returned by time()
     */
    public static $now;
    
    private $user_id = 2;
    private $token = 'TOKEN123';
    
    public function testNonceField()
    {
        $wp_nonces = new WpNonces($this->user_id, $this->token, '_wpnonce');
        $wp_nonces->wp_nonce_field('action_123');
        
        $output = $this->getActualOutput();
        $str = 'name="_wpnonce" value="' . $wp_nonces->wp_create_nonce( 'action_123' ) . '"';
        
        $this->assertContains($str, $output);
    }
    
    public function testNonceUrl()
    {
        $wp_nonces = new WpNonces($this->user_id, $this->token, '_wpnonce');
        
        $action_url = 'comment.php?action=someaction&arg1=somearg';
        
        $url = $wp_nonces->wp_nonce_url($action_url, 'delete-comment_1');
        
        $param = '&amp;_wpnonce='.$wp_nonces->wp_create_nonce('delete-comment_1');
        
        $this->assertContains($param, $url);
    }
    
    public function testNoncesExpireDate()
    {
        $wp_nonces = new WpNonces($this->user_id, $this->token, '_wpnonce');
        
        $nonce = $wp_nonces->wp_create_nonce('action_123');
        
        $_REQUEST['_wpnonce'] = $nonce;
        
        $res = $wp_nonces->wp_verify_nonce('action_123');
        $this->assertGreaterThanOrEqual(1, $res);
        
        self::$now = \time() + intval(WpNonces::DAY_IN_SECONDS*0.2);
        $res = $wp_nonces->wp_verify_nonce('action_123');
        $this->assertGreaterThanOrEqual(1, $res);
        
        self::$now = \time() + intval(WpNonces::DAY_IN_SECONDS*0.5);
        $res = $wp_nonces->wp_verify_nonce('action_123');
        $this->assertGreaterThanOrEqual(1, $res);
        
        self::$now = \time() + intval(WpNonces::DAY_IN_SECONDS*1.1);
        $res = $wp_nonces->wp_verify_nonce('action_123');
        $this->assertFalse($res);
    }
}
