<?php

namespace Vdmytriv\Wordpress;

/**
 * Wordpress wp_nonce_*() functions in OOP way
 * 
 */
class WpNonces
{
    
    const DAY_IN_SECONDS = 24 * 60 * 60;
    const NONCE_LIFE = self::DAY_IN_SECONDS;
    const HASH_KEY = 'TEST@#$23401234567890TEST';
    
    private $user_id;
    private $token;
    private $name;
    
    /**
     * Constructor
     * 
     * @param type $user_id User ID
     * @param type $token Access token
     * @param type $name field name with nonce value
     */
    public function __construct($user_id, $token, $name = "_wpnonce")
    {
        $this->user_id = $user_id;
        $this->token = $token;
        $this->name = $name;
    }
    
    /**
     * Retrieve or display nonce hidden field for forms.
     * 
     * @param type $action Optional. Action name. Default -1.
     * @param type $echo Optional. Whether to display or return hidden form field. Default true.
     * @return string
     */
    public function wp_nonce_field($action = -1, $echo = true)
    {
        $nonce_field = '<input type="hidden" id="' . $this->name . '" name="' . $this->name . '" value="' . $this->wp_create_nonce($action) . '" />';
        
        if ($echo) {
            echo $nonce_field;
        }
        
        return $nonce_field;
    }
    
    /**
     * Retrieve URL with nonce added to URL query.
     * 
     * @param string $actionurl URL to add nonce.
     * @param int|string $action Optional. Nonce action name. Default -1.
     * @return string Escaped URL with nonce added.
     */
    public function wp_nonce_url($actionurl, $action = -1) 
    {
        $actionurl = str_replace( '&amp;', '&', $actionurl );
        
        if(strpos($actionurl, '?')) {
            $actionurl .= '&';
        }
        else {
            $actionurl .= '?';
        }
        
        $actionurl .= $this->name . '=' . $this->wp_create_nonce( $action );
        $actionurl = str_replace( '&', '&amp;', $actionurl );
        
        return $actionurl;
    }
    
    /**
     * Creates a cryptographic token tied to a specific action, user, user session,
     * and window of time.
     * 
     * @param string|int $action Scalar value to add context to the nonce.
     * @return string The token.
     */
    public function wp_create_nonce($action = -1)
    {
        $i = $this->wp_nonce_tick();
        $data = $i . '|' . $action . '|' . $this->user_id . '|' . $this->token;
        
        return substr( $this->wp_hash( $data, 'nonce' ), -12, 10 );
    }
    
    /**
    * Verify that correct nonce was used with time limit.
    *
    * @param string|int $action Should give context to what is taking place and be the same when nonce was created.
    * @return false|int False if the nonce is invalid, 1 if the nonce is valid and generated between
    *                   0-12 hours ago, 2 if the nonce is valid and generated between 12-24 hours ago.
    */
    public function wp_verify_nonce($action = -1) 
    {
        if(!isset($_REQUEST[$this->name])) {
            return false;
        }
        
        $nonce = (string) $_REQUEST[$this->name];
        $uid = $this->user_id;

        if ( empty( $nonce ) ) {
            return false;
        }

        $token = $this->token;
        $i = $this->wp_nonce_tick();
        
        // Nonce generated 0-12 hours ago
        $expected = substr( $this->wp_hash( $i . '|' . $action . '|' . $uid . '|' . $token, 'nonce'), -12, 10 );
        if ( hash_equals( $expected, $nonce ) ) {
            return 1;
        }

        // Nonce generated 12-24 hours ago
        $expected = substr( $this->wp_hash( ( $i - 1 ) . '|' . $action . '|' . $uid . '|' . $token, 'nonce' ), -12, 10 );
        if ( hash_equals( $expected, $nonce ) ) {
            return 2;
        }

        // Invalid nonce
        return false;
    }
    
    private function wp_hash($data, $scheme = 'auth')
    {
        if(function_exists('wp_hash')) {
            return wp_hash($data, $scheme);
        }
        
        return $this->_hash_hmac($data);
    }
    
    private function _hash_hmac($data)
    {
        $algo = 'md5';
        $key = self::HASH_KEY;
        $pack = 'H32';

        if (strlen($key) > 64) {
            $key = pack($pack, $algo($key));
        }

        $key = str_pad($key, 64, chr(0));

        $ipad = (substr($key, 0, 64) ^ str_repeat(chr(0x36), 64));
        $opad = (substr($key, 0, 64) ^ str_repeat(chr(0x5C), 64));

        $hmac = $algo($opad . pack($pack, $algo($ipad . $data)));

        return $hmac;
    }
    
    private function wp_nonce_tick()
    {
        return ceil(time() / ( self::NONCE_LIFE / 2 ));
    }
    
}
