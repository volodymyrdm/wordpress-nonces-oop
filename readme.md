# Wordpress nonce OOP
This package will allow you to use nonces in your wordpress site using object oriented way.

## Features
* Add nonces to url
* Add nonces to html form
* Nonce verification

## Installation
Add new composer package:

```
composer require vdmytriv/wordpress-nonces-oop
```

Then update composer:

```
composer update
```

## Usage
Add the namespace and create new instance of WpNonces:

```
use Vdmytriv\Wordpress\WpNonces;

$wp_nonces = new WpNonces(USER_ID, ACCESS_TOKEN);
```

Now you can add nonce field to html form:

```
$wp_nonces->wp_nonce_field('delete-comment_1')
```

To add nonce field to url use wp_nonce_url() function:

```
$wp_nonces->wp_nonce_url(URL, 'delete-comment_1')
```

To validate nonce field when you process form data call this function:

```
$wp_nonces->wp_verify_nonce('delete-comment_1')
```

if nonce is valid wp_verify_nonce function returns 1 or 2.
If nonce is invalid it returns FALSE;
Nonce is valid when it is not older then one day.

